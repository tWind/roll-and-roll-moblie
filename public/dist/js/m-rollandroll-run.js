function setCatalogueBehavior() {
	var options = {
		block: '.js-catalogue',
		links: '.js-catalogue-add',
		modal: '.js-catalogue-modal'
	}

	var $block = $(options.block);
	var $links = $block.find(options.links);
	var $modal = $block.find(options.modal);



	$links.on('click', function(e) {
		e.preventDefault();

		var $parent = $(this).parent();
		$links.parent().not($parent).removeClass('active');

		$parent.toggleClass('active');
	});

	$(document).click(function(e) {
		var $el = $(event.target);

		if ($el.closest(options.block).length) return;

		$($links.parent()).removeClass('active');

        e.stopPropagation();
	});

	$('.js-scroll a').on('click', function(e) {
		e.preventDefault();
		$(this).toggleClass('ok');
	})

	$modal.find('.js-scroll').jScrollPane();
};
(function() {
	function counter(el) {
		var counter = {};

		var settings = {
			collapse: true
		};


		counter.init = function() {
			this.$block = $(el);
			this.$add = this.$block.find('.js-counter-add');
			this.$remove = this.$block.find('.js-counter-remove');
			this.$viewport = this.$block.find('.js-counter-result');

			this.options = $.extend({}, settings, this.$block.data('counter-options'));
			this.result = 0;

			this.applyBindings();
			counter.set();
		}
		// можно уменьшить до одного обработчика... но зачем? :D
		counter.applyBindings = function() {
			var self = this;

			this.$add.on('click', function() {
				self.result += 1;
				self.set();
			});
			this.$remove.on('click', function() {
				if(self.result > 0)
					self.result -= 1;
				self.set();
			});
		};

		counter.set = function() {
			this.result == 0 && this.options.collapse
				? this.$block.addClass('collapse')
				: this.$block.removeClass('collapse');

			this.$viewport.text(this.result);
		};

		counter.init();

		return counter;
	};

	$.each($('.js-counter'), function() {
		counter(this);
	});


})(jQuery);
// Global JS file
(function() {
	$(document).ready(function() {
	    $('.menutop ul li').click(function() {
	        $.arcticmodal({
	            type: 'ajax',
	            url: $(this).children('a').attr('href'),
	            afterLoading: function(data, el) {
	                $(".arcticmodal-container").css({
	                	top: screen.height,
	                	overflow: 'hidden'
	                });

	            },
	            afterLoadingOnShow: function(data, el) {
	                $(".arcticmodal-container").animate({
	                    top: "-=" + screen.height,
	                }, 1200, function() {
	                	$(".arcticmodal-container").css({
		                	overflow: 'auto'
		                });
	                });

	                bindCloseModal();
	                setCatalogueBehavior();
	                bindSelect();
	                counter();
	                showMenu();

	                switcher();
	            },
	            beforeClose: function(data, el) {
	            	$(".arcticmodal-container").css({ overflow: 'hidden' });
	                $(".arcticmodal-container").animate({
	                    top: "+=" + screen.height,
	                }, 1200);
	            }
	        });
	    });
	});
	function bindCloseModal() {
		$('.js-close_modal').on('click', function() {
        	$('.arcticmodal-overlay').trigger('click');
    	});
	}
})();


function showMenu() {
	var $block = $('.b-nav');
	var $links = $block.find('a');

	$links.on('click', function(e) {
		e.preventDefault();
	});

	$(window).on('scroll', function() {

		var top = $(document).scrollTop();
		if (top > 268)
			$('.menutop').addClass('fixed');
		else
			$('.menutop').removeClass('fixed');
	});

	$('.arcticmodal-container').on('scroll', function() {
		var top = $(this).scrollTop();
		if (top > 268) {
			$('.menutop').addClass('fixed');
			$('.js-fix').addClass('fixed');
		}
		else {
			$('.menutop').removeClass('fixed');
			$('.js-fix').removeClass('fixed');
		}
	});
};

showMenu();

function bindSelect() {
	var $select = $('.js-select').find('select');
	$select.select2({
		minimumResultsForSearch: -1
	});
};
(function() {

	var options = {
		center: true,
	    items:2,
	    loop:true,
	    margin:25,
	    dots: false
  	}

  	var $block = $(".js-slider");

	$.each($block, function() {
		var $el = $(this);
		var settings = $el.data('slider-settings');

		$el.owlCarousel($.extend({}, options, settings));
	});

 })();