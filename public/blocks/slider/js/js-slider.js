(function() {

	var options = {
		center: true,
	    items:2,
	    loop:true,
	    margin:25,
	    dots: false
  	}

  	var $block = $(".js-slider");

	$.each($block, function() {
		var $el = $(this);
		var settings = $el.data('slider-settings');

		$el.owlCarousel($.extend({}, options, settings));
	});

 })();