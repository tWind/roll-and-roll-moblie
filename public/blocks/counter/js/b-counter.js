(function() {
	function counter(el) {
		var counter = {};

		var settings = {
			collapse: true
		};


		counter.init = function() {
			this.$block = $(el);
			this.$add = this.$block.find('.js-counter-add');
			this.$remove = this.$block.find('.js-counter-remove');
			this.$viewport = this.$block.find('.js-counter-result');

			this.options = $.extend({}, settings, this.$block.data('counter-options'));
			this.result = 0;

			this.applyBindings();
			counter.set();
		}
		// можно уменьшить до одного обработчика... но зачем? :D
		counter.applyBindings = function() {
			var self = this;

			this.$add.on('click', function() {
				self.result += 1;
				self.set();
			});
			this.$remove.on('click', function() {
				if(self.result > 0)
					self.result -= 1;
				self.set();
			});
		};

		counter.set = function() {
			this.result == 0 && this.options.collapse
				? this.$block.addClass('collapse')
				: this.$block.removeClass('collapse');

			this.$viewport.text(this.result);
		};

		counter.init();

		return counter;
	};

	$.each($('.js-counter'), function() {
		counter(this);
	});


})(jQuery);